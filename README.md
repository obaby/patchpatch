This is Patch Patch. 

Description: A sample iOS project, that patches an OLD file, by using a patch file (created with bsdiff), in order to create an updated version of it (NEW file).
	Binarys are ALSO supported. In fact thats why I needed this project for.

This project offers the patching part ONLY (patch apply) and not the diff part (patch creation) (therefore it assumes you already HAVE the patch file).  You can use any BSDiff  (http://www.daemonology.net/bsdiff/) tool to create the patch file. In our case we used Multitouch (http://projects.sappharad.com/tools/multipatch.html).


How to use it: 
	Its use is quite simple. 
	Just observe the code in ViewController.m files onClick function and you will understand (There are comments).


Use case:
	We have a client application and we've got a server.
	Client has a file with version 1.0.
	Server has a file with version 2.0
	Till now the client had to download the WHOLE version 2.0 file to update itself. Now we can create a patch file (which is much smaller in size), and patch the clients version 1.0 file, thus saving bandwith.


In our sample example we've got 2 files.
	1) The old file (oldFile.txt) which contains: 
		"Hello. This is a test."
	2) The patch file which was created with Multitouch.

The resulting file (newFile.txt) SHOULD contain the following (if all goes well):
	"Hello I have changed some things. This a test."
It is to be found at 
 ~/Library/Application Support/iPhone Simulator/{SIMULATOR_VERSION}/Applications/{APPLICATION_ID}/Documents/newFile.txt ( if running on simulator) 
 OR in 
 ApplicationDir/Documents/newFile.txt (if running on a device).
 I've tested this app with 100mb binaries and it worked just fine.


All the library implementation credits go to GroupCommerce which built their awesome GCBSDiffKit project. (https://github.com/GroupCommerce/GCBSDiffKit). 
We got the bzip2lib from dingtianran. (https://github.com/dingtianran/LibBzip2)

